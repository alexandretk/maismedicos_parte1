package view;

import model.*;
import controller.CadastroConsulta;

import javax.swing.JOptionPane;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.JPanel;

import java.awt.BorderLayout;

import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JTextField;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JTextPane;
import javax.swing.text.StyledDocument;

public class TelaConsulta {

	private JFrame frame;
	private JTextField txtNomePaciente;
	private JTextField txtIdadePaciente;
	private JTextField txtPesoPaciente;
	private JTextField txtNomeMedico;
	private JTextField txtPlanoSaudePaciente;
	private JTextField txtDoencaPaciente;
	private JTextField txtInicioTratamentoPaciente;
	private JTextField txtMedicamento;
	private JTextField txtMedicamentoInd;
	private JTextField txtIdadeMedico;
	private JTextField txtPesoMedico;
	private JTextField txtCRMMedico;
	private JTextField txtAnosExpMedico;
	
	private JTextField txtHospitalMedico;
	private JTextField txtNumConsulta;
	private JTextField txtPaciente;
	private JTextField txtMedico;
	private JTextField txtDiagnostico;
	private JTextField txtDia;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaConsulta window = new TelaConsulta();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TelaConsulta() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		final CadastroConsulta cadastro = new CadastroConsulta();
		
		frame = new JFrame();
		frame.setBounds(100, 100, 530, 383);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		frame.getContentPane().add(tabbedPane, BorderLayout.CENTER);
		
		final JPanel MenuInicial = new JPanel();
		tabbedPane.addTab("Menu Inicial", null, MenuInicial, null);
		tabbedPane.setEnabledAt(0, false);
		MenuInicial.setLayout(null);
		
		JLabel lblInicio = new JLabel("Programa Cadastro de Consulta");
		lblInicio.setFont(new Font("Dialog", Font.BOLD, 20));
		lblInicio.setBounds(70, 36, 372, 37);
		MenuInicial.add(lblInicio);
		
		JLabel Escolha = new JLabel("Escolha o que deseja realizar e clique na tab correspondente!");
		Escolha.setBounds(43, 111, 470, 15);
		MenuInicial.add(Escolha);
		
		JLabel lblFeitoPor = new JLabel("Feito por:");
		lblFeitoPor.setBounds(350, 274, 70, 15);
		MenuInicial.add(lblFeitoPor);
		
		JLabel lblAlexandreTorresKryonidis = new JLabel("Alexandre Torres Kryonidis, 13/0099767");
		lblAlexandreTorresKryonidis.setBounds(237, 301, 276, 15);
		MenuInicial.add(lblAlexandreTorresKryonidis);
		
		JPanel Cadastrar = new JPanel();
		tabbedPane.addTab("Cadastrar", null, Cadastrar, null);
		Cadastrar.setLayout(null);
		

		final JPanel panel_5 = new JPanel();
		panel_5.setBounds(473, 12, 40, 38);
		Cadastrar.add(panel_5);
		panel_5.setLayout(null);
		panel_5.setVisible(false);
		
		JLabel nomePaciente = new JLabel("Nome:");
		nomePaciente.setBounds(23, 12, 51, 15);
		panel_5.add(nomePaciente);
		
		txtNomePaciente = new JTextField();
		txtNomePaciente.setBounds(80, 10, 206, 19);
		panel_5.add(txtNomePaciente);
		txtNomePaciente.setColumns(10);
		
		JLabel idadePaciente = new JLabel("Idade:");
		idadePaciente.setBounds(23, 35, 51, 15);
		panel_5.add(idadePaciente);
		
		txtIdadePaciente = new JTextField();
		txtIdadePaciente.setColumns(10);
		txtIdadePaciente.setBounds(80, 35, 206, 19);
		panel_5.add(txtIdadePaciente);
		
		txtPesoPaciente = new JTextField();
		txtPesoPaciente.setColumns(10);
		txtPesoPaciente.setBounds(80, 60, 206, 19);
		panel_5.add(txtPesoPaciente);
		
		JLabel pesoPaciente = new JLabel("Peso:");
		pesoPaciente.setBounds(28, 60, 51, 15);
		panel_5.add(pesoPaciente);
		
		txtPlanoSaudePaciente = new JTextField();
		txtPlanoSaudePaciente.setColumns(10);
		txtPlanoSaudePaciente.setBounds(135, 85, 151, 19);
		panel_5.add(txtPlanoSaudePaciente);
		
		JLabel planoSaudePaciente = new JLabel("Plano Saude:");
		planoSaudePaciente.setBounds(34, 85, 94, 15);
		panel_5.add(planoSaudePaciente);
		
		JLabel doencaPaciente = new JLabel("Doenca:");
		doencaPaciente.setBounds(70, 110, 65, 15);
		panel_5.add(doencaPaciente);
		
		txtDoencaPaciente = new JTextField();
		txtDoencaPaciente.setColumns(10);
		txtDoencaPaciente.setBounds(135, 110, 151, 19);
		panel_5.add(txtDoencaPaciente);
		
		JLabel InicioTratamentoPaciente = new JLabel("Inicio Tratamento:");
		InicioTratamentoPaciente.setBounds(0, 135, 130, 15);
		panel_5.add(InicioTratamentoPaciente);
		
		txtInicioTratamentoPaciente = new JTextField();
		txtInicioTratamentoPaciente.setColumns(10);
		txtInicioTratamentoPaciente.setBounds(135, 135, 151, 19);
		panel_5.add(txtInicioTratamentoPaciente);
		
		JLabel medicamentoPaciente = new JLabel("Medicamento:");
		medicamentoPaciente.setBounds(30, 160, 105, 15);
		panel_5.add(medicamentoPaciente);
		
		txtMedicamentoInd = new JTextField();
		txtMedicamentoInd.setColumns(10);
		txtMedicamentoInd.setBounds(135, 160, 151, 19);
		panel_5.add(txtMedicamentoInd);
		
		final JPanel panel_6 = new JPanel();
		panel_6.setBounds(215, 12, 34, 38);
		Cadastrar.add(panel_6);
		panel_6.setLayout(null);
		panel_6.setVisible(false);
		
		JLabel nomeMedico = new JLabel("Nome:");
		nomeMedico.setBounds(23, 12, 51, 15);
		panel_6.add(nomeMedico);
		
		txtNomeMedico = new JTextField();
		txtNomeMedico.setBounds(80, 10, 206, 19);
		panel_6.add(txtNomeMedico);
		txtNomePaciente.setColumns(10);
		
		JButton btnSubmeterPaciente = new JButton("Submeter Paciente");
		btnSubmeterPaciente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Paciente umPaciente = new Paciente(txtNomePaciente.getText(), Integer.parseInt(txtIdadePaciente.getText()), Float.parseFloat(txtPesoPaciente.getText()), txtPlanoSaudePaciente.getText() );
				umPaciente.setDoenca(txtDoencaPaciente.getText());
				umPaciente.setInicioTratamento(txtInicioTratamentoPaciente.getText());
				umPaciente.setMedicamentoUtilizado(txtMedicamentoInd.getText());
				cadastro.adicionarPaciente(umPaciente);
				JOptionPane.showMessageDialog(null, "Paciente cadastrado com sucesso!");
				
				txtNomePaciente.setText("");
				txtIdadePaciente.setText("");
				txtPesoPaciente.setText("");
				txtPlanoSaudePaciente.setText("");
				txtDoencaPaciente.setText("");
				txtMedicamentoInd.setText("");
				txtInicioTratamentoPaciente.setText("");
			}
		});
		btnSubmeterPaciente.setBounds(70, 213, 169, 25);
		panel_5.add(btnSubmeterPaciente);
		
		JLabel idadeMedico = new JLabel("Idade:");
		idadeMedico.setBounds(23, 35, 51, 15);
		panel_6.add(idadeMedico);
		
		txtIdadeMedico = new JTextField();
		txtIdadeMedico.setColumns(10);
		txtIdadeMedico.setBounds(80, 35, 206, 19);
		panel_6.add(txtIdadeMedico);
		
		txtPesoMedico = new JTextField();
		txtPesoMedico.setColumns(10);
		txtPesoMedico.setBounds(80, 60, 206, 19);
		panel_6.add(txtPesoMedico);
		
		JLabel pesoMedico = new JLabel("Peso:");
		pesoMedico.setBounds(28, 60, 51, 15);
		panel_6.add(pesoMedico);
		
		txtCRMMedico = new JTextField();
		txtCRMMedico.setColumns(10);
		txtCRMMedico.setBounds(135, 85, 151, 19);
		panel_6.add(txtCRMMedico);
		
		JLabel crmMedico = new JLabel("CRM:");
		crmMedico.setBounds(90, 85, 36, 15);
		panel_6.add(crmMedico);
		
		JLabel AnosExpMedico = new JLabel("Anos Experiencia:");
		AnosExpMedico.setBounds(2, 110, 126, 15);
		panel_6.add(AnosExpMedico);
		
		txtAnosExpMedico = new JTextField();
		txtAnosExpMedico.setColumns(10);
		txtAnosExpMedico.setBounds(135, 110, 151, 19);
		panel_6.add(txtAnosExpMedico);
		
		JLabel HospitalMedico = new JLabel("Hospital Trabalho:");
		HospitalMedico.setBounds(0, 139, 151, 15);
		panel_6.add(HospitalMedico);
		
		txtHospitalMedico = new JTextField();
		txtHospitalMedico.setColumns(10);
		txtHospitalMedico.setBounds(135, 135, 151, 19);
		panel_6.add(txtHospitalMedico);
		
		JButton btnSubmeterMedico = new JButton("Submeter Medico");
		btnSubmeterMedico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Medico umMedico = new Medico(txtNomeMedico.getText(), Integer.parseInt(txtIdadeMedico.getText()), Float.parseFloat(txtPesoMedico.getText()), txtCRMMedico.getText() );
				umMedico.setHospitalTrabalho(txtHospitalMedico.getText());
				umMedico.setAnosExperiencia(Integer.parseInt(txtAnosExpMedico.getText()));
				cadastro.adicionarMedico(umMedico);
				JOptionPane.showMessageDialog(null, "Medico cadastrado com sucesso!");
				txtNomeMedico.setText("");
				txtIdadeMedico.setText("");
				txtPesoMedico.setText("");
				txtCRMMedico.setText("");
				txtHospitalMedico.setText("");
				txtAnosExpMedico.setText("");
			}
		});
		btnSubmeterMedico.setBounds(70, 213, 169, 25);
		panel_6.add(btnSubmeterMedico);
		
		
		final JPanel panel_7 = new JPanel();
		panel_7.setBounds(215, 12, 298, 304);
		Cadastrar.add(panel_7);
		panel_7.setLayout(null);
		panel_7.setVisible(false);		
		
		JLabel numeroConsulta = new JLabel("Numero da Consulta:");
		numeroConsulta.setBounds(0, 62, 149, 15);
		panel_7.add(numeroConsulta);

		txtNumConsulta = new JTextField();
		txtNumConsulta.setColumns(10);
		txtNumConsulta.setBounds(152, 62, 134, 19);
		panel_7.add(txtNumConsulta);
		

		JLabel paciente = new JLabel("Paciente:");
		paciente.setBounds(2, 12, 72, 15);
		panel_7.add(paciente);
		
		txtPaciente = new JTextField();
		txtPaciente.setColumns(10);
		txtPaciente.setBounds(80, 12, 206, 19);
		panel_7.add(txtPaciente);
		

		JLabel medico = new JLabel("Medico:");
		medico.setBounds(2, 37, 77, 15);
		panel_7.add(medico);

		txtMedico = new JTextField();
		txtMedico.setColumns(10);
		txtMedico.setBounds(80, 37, 206, 19);
		panel_7.add(txtMedico);
		
		
		JLabel diagnostico = new JLabel("Diagnostico:");
		diagnostico.setBounds(59, 87, 101, 15);
		panel_7.add(diagnostico);
		
		txtDiagnostico = new JTextField();
		txtDiagnostico.setColumns(10);
		txtDiagnostico.setBounds(152, 87, 134, 19);
		panel_7.add(txtDiagnostico);

		
		JLabel dia = new JLabel("Dia consulta:");
		dia.setBounds(57, 112, 101, 15);
		panel_7.add(dia);
		
		txtDia = new JTextField();
		txtDia.setColumns(10);
		txtDia.setBounds(152, 112, 134, 19);
		panel_7.add(txtDia);
		

		JLabel medicamento = new JLabel("Medicamento Indicado:");
		medicamento.setFont(new Font("Dialog", Font.BOLD, 11));
		medicamento.setBounds(0, 137, 176, 15);
		panel_7.add(medicamento);
		
		txtMedicamento = new JTextField();
		txtMedicamento.setColumns(10);
		txtMedicamento.setBounds(152, 135, 134, 19);
		panel_7.add(txtMedicamento);
		
		JButton btnCadastrarConsulta = new JButton("Cadastrar Consulta");
		btnCadastrarConsulta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean ok=true;
				Paciente umPaciente = cadastro.pesquisarPaciente(txtPaciente.getText());
				Medico umMedico=cadastro.pesquisarMedico(txtMedico.getText());
				if (umPaciente==null)
				{
					JOptionPane.showMessageDialog(null, "Paciente nao cadastrado, cadastre-o!");
					ok=false;
				}
				if (umMedico==null)
				{
					JOptionPane.showMessageDialog(null, "Medico nao cadastrado, cadastre-o!");
					ok=false;
				}	
				if (ok)
				{
					ConsultaMedica umaConsulta=new ConsultaMedica();
					cadastro.inserirDadosConsulta(umaConsulta, Integer.parseInt(txtNumConsulta.getText()), umMedico, umPaciente, txtDiagnostico.getText());
					umaConsulta.setDia(txtDia.getText());
					umaConsulta.setMedicamentoIndicado(txtMedicamento.getText());
					cadastro.adicionarConsulta(umaConsulta);
					
					JOptionPane.showMessageDialog(null, "Consulta cadastrada com sucesso!");
					txtNumConsulta.setText("");
					txtDiagnostico.setText("");
					txtPaciente.setText("");
					txtMedico.setText("");
					txtDia.setText("");
					txtMedicamento.setText("");
				}
			}
		});
		btnCadastrarConsulta.setBounds(70, 213, 176, 25);
		panel_7.add(btnCadastrarConsulta);
		
		
		
		
		JButton btnCadastrarPaciente = new JButton("Cadastrar Paciente");
		btnCadastrarPaciente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panel_7.setVisible(false);
				panel_6.setVisible(false);
				panel_5.setVisible(true);
				panel_5.setBounds(215, 12, 298, 304);
				
			}
		});
		
		btnCadastrarPaciente.setBounds(12, 40, 183, 25);
		Cadastrar.add(btnCadastrarPaciente);
		
		JButton btnCadastrarMedico = new JButton("Cadastrar Medico");
		btnCadastrarMedico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel_5.setVisible(false);
				panel_7.setVisible(false);
				panel_6.setVisible(true);
				panel_6.setBounds(215, 12, 298, 304);
				

			}
		});
		btnCadastrarMedico.setBounds(12, 89, 183, 25);
		Cadastrar.add(btnCadastrarMedico);
		
		JButton button = new JButton("Cadastrar Consulta");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel_5.setVisible(false);
				panel_6.setVisible(false);
				panel_7.setVisible(true);
				panel_7.setBounds(215, 12, 298, 304);
			}
		});
		button.setBounds(12, 136, 183, 25);
		Cadastrar.add(button);
		
		
		JPanel ConsultarDados = new JPanel();
		tabbedPane.addTab("Consultar Dados", null, ConsultarDados, null);
		ConsultarDados.setLayout(null);
		
		final JTextPane textPane = new JTextPane();
		textPane.setBounds(12, 112, 233, 189);
		ConsultarDados.add(textPane);
		
		JButton btnListarPacientesCadastrados = new JButton("Listar Pacientes Cadastrados");
		btnListarPacientesCadastrados.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				textPane.setText("");
			StyledDocument doc = textPane.getStyledDocument();
			try {
				doc.insertString(doc.getLength(),cadastro.exibirPacientes(), null);

			} 
			catch(Exception e) { JOptionPane.showMessageDialog(null, "Erro");}
			
			}
		});
		btnListarPacientesCadastrados.setBounds(7, 12, 244, 25);
		ConsultarDados.add(btnListarPacientesCadastrados);
		
		JButton button_1 = new JButton("Listar Medicos Cadastrados");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				textPane.setText("");
			StyledDocument doc = textPane.getStyledDocument();
			try {
				doc.insertString(doc.getLength(),cadastro.exibirMedicos(), null);

			} 
			catch(Exception e) { JOptionPane.showMessageDialog(null, "Erro");}
			
			}
		});
		button_1.setBounds(7, 47, 244, 25);
		ConsultarDados.add(button_1);
		
		JButton button_2 = new JButton("Listar Numero das Consultas");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				textPane.setText("");
			StyledDocument doc = textPane.getStyledDocument();
			try {
				doc.insertString(doc.getLength(),cadastro.exibirConsulta(), null);

			} 
			catch(Exception e) { JOptionPane.showMessageDialog(null, "Erro");}
			
			}
		});
		button_2.setBounds(7, 82, 244, 25);
		ConsultarDados.add(button_2);
		
		JButton btnBuscarPaciente = new JButton("Buscar Paciente");
		btnBuscarPaciente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String umNome = JOptionPane.showInputDialog(null,"Digite o nome do Paciente que deseja buscar",	"Buscar Paciente", JOptionPane.OK_OPTION);
				Paciente umPaciente = cadastro.pesquisarPaciente(umNome);
				if (umPaciente!=null){
					JOptionPane.showMessageDialog(null, cadastro.caractPaciente(umPaciente));
				} else {
					JOptionPane.showMessageDialog(null, "Paciente não encontrado");
				}
			}
		});
		btnBuscarPaciente.setBounds(316, 100, 148, 25);
		ConsultarDados.add(btnBuscarPaciente);
		
		JButton button_3 = new JButton("Buscar Medico");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String umNome = JOptionPane.showInputDialog(null,"Digite o nome do Paciente que deseja buscar",	"Buscar Paciente", JOptionPane.OK_OPTION);
				Medico umMedico = cadastro.pesquisarMedico(umNome);
				if (umMedico!=null){
					JOptionPane.showMessageDialog(null, cadastro.caractMedico(umMedico));
				} else {
					JOptionPane.showMessageDialog(null, "Medico não encontrado");
				}
			}
		});
		button_3.setBounds(316, 150, 148, 25);
		ConsultarDados.add(button_3);
		
		JButton button_4 = new JButton("Buscar Consulta");
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
				int umNumero = Integer.parseInt(JOptionPane.showInputDialog(null,"Digite o numero da consulta que deseja buscar",	"Buscar Consulta", JOptionPane.OK_OPTION));
				ConsultaMedica umaConsulta = cadastro.pesquisarConsulta(umNumero);
				if (umaConsulta!=null){
					JOptionPane.showMessageDialog(null, cadastro.caractConsulta(umaConsulta));
				} else {
					JOptionPane.showMessageDialog(null, "Consulta não encontrada");
				}
			}
		});
		button_4.setBounds(316, 200, 149, 25);
		ConsultarDados.add(button_4);
		
		JPanel Editar = new JPanel();
		tabbedPane.addTab("Editar/Remover", null, Editar, null);
		Editar.setLayout(null);
		
		JButton btnRemoverPaciente = new JButton("Remover Paciente");
		btnRemoverPaciente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String umNome = JOptionPane.showInputDialog(null,"Digite o nome do Paciente a ser Removido do Cadastro",	"Remover Paciente", JOptionPane.OK_OPTION);
				Paciente umPaciente = cadastro.pesquisarPaciente(umNome);
				if (umPaciente!=null){
				cadastro.removerPaciente(umPaciente);
				JOptionPane.showMessageDialog(null, "Paciente Removido com sucesso");
				} else {
					JOptionPane.showMessageDialog(null, "Paciente não encontrado");
				}
				
			}
		});
		btnRemoverPaciente.setBounds(156, 43, 196, 65);
		Editar.add(btnRemoverPaciente);
		
		JButton btnRemoverMedico = new JButton("Remover Medico");
		btnRemoverMedico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String umNome = JOptionPane.showInputDialog(null,"Digite o nome do Medico a ser Removido do Cadastro",	"Remover Medico", JOptionPane.OK_OPTION);
				Medico umMedico = cadastro.pesquisarMedico(umNome);
				if (umMedico!=null){
				cadastro.removerMedico(umMedico);
				JOptionPane.showMessageDialog(null, "Medico Removido com sucesso");
				} else {
					JOptionPane.showMessageDialog(null, "Medico não encontrado");
				}
			}
		});
		btnRemoverMedico.setBounds(156, 124, 196, 65);
		Editar.add(btnRemoverMedico);
		
		JButton btnRemoverConsulta = new JButton("Remover Consulta");
		btnRemoverConsulta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				int umNumero = Integer.parseInt(JOptionPane.showInputDialog(null,"Digite o numero da consulta a ser Removida do Cadastro",	"Remover Consulta", JOptionPane.OK_OPTION));
				ConsultaMedica umaConsulta = cadastro.pesquisarConsulta(umNumero);
				if (umaConsulta!=null){
				cadastro.removerConsulta(umaConsulta);
				JOptionPane.showMessageDialog(null, "Consulta Removida com sucessa");
				} else {
					JOptionPane.showMessageDialog(null, "Consulta não encontrada");
				}
			}
		});
		btnRemoverConsulta.setBounds(156, 207, 196, 65);
		Editar.add(btnRemoverConsulta);
	}
}
