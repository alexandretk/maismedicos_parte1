package model;

	import model.*;

public class ConsultaMedica {
	
	
	private int numero;
	private Paciente paciente;
	private Medico medico;
	private String diagnostico;
	private String dia;	
	private String medicamentoIndicado;
	
	
	public Paciente getPaciente() {
		return paciente;
	}
	
	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}
	
	public Medico getMedico() {
		return medico;
	}
	
	public void setMedico(Medico medico) {
		this.medico = medico;
	}
	
	public String getDiagnostico() {
		return diagnostico;
	}
	
	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}
	
	public String getDia() {
		return dia;
	}
	
	public void setDia(String dia) {
		this.dia = dia;
	}
	
	public String getMedicamentoIndicado() {
		return medicamentoIndicado;
	}
	
	public void setMedicamentoIndicado(String medicamentoIndicado) {
		this.medicamentoIndicado = medicamentoIndicado;
	}
	
	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	
}
