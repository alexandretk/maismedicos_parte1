package model;

public class Paciente extends Usuario{


	private String planoSaude;
	private String doenca;	
	private String inicioTratamento;
	private String medicamentoUtilizado;
	
	
	
	public Paciente(String nome, int idade, float peso, String planoSaude) {
		super(nome, idade, peso);
		this.planoSaude = planoSaude;
	}

	public String getDoenca() {
		return doenca;
	}
	
	public void setDoenca(String doenca) {
		this.doenca = doenca;
	}
	
	public String getInicioTratamento() {
		return inicioTratamento;
	}
	
	public void setInicioTratamento(String inicioTratamento) {
		this.inicioTratamento = inicioTratamento;
	}
	
	public String getMedicamentoUtilizado() {
		return medicamentoUtilizado;
	}
	
	public void setMedicamentoUtilizado(String medicamentoUtilizado) {
		this.medicamentoUtilizado = medicamentoUtilizado;
	}

	public String getPlanoSaude() {
		return planoSaude;
	}

	public void setPlanoSaude(String planoSaude) {
		this.planoSaude = planoSaude;
	}
	
	
}
