package model;

public class Medico extends Usuario{

	private String crm;
	private int anosExperiencia;
	private String hospitalTrabalho;
	
	
	
	
	public Medico(String nome, int idade, float peso, String crm) {
		super(nome, idade, peso);
		this.crm = crm;
	}
	

	public String getCrm() {
		return crm;
	}
	
	public void setCrm(String crm) {
		this.crm = crm;
	}
	
	public int getAnosExperiencia() {
		return anosExperiencia;
	}
	
	public void setAnosExperiencia(int anosExperiencia) {
		this.anosExperiencia = anosExperiencia;
	}
	
	public String getHospitalTrabalho() {
		return hospitalTrabalho;
	}
	
	public void setHospitalTrabalho(String hospitalTrabalho) {
		this.hospitalTrabalho = hospitalTrabalho;
	}
	
	
}
