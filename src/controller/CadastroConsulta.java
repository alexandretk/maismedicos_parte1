package controller;

import model.*;
import java.util.ArrayList;


public class CadastroConsulta {

	ArrayList <Paciente> listaPacientes;
	ArrayList <Medico> listaMedicos;
	ArrayList <ConsultaMedica> listaConsulta;
	
	public CadastroConsulta(){
		listaPacientes = new ArrayList<Paciente>();
		listaMedicos = new ArrayList<Medico>();
		listaConsulta = new ArrayList<ConsultaMedica>();
	}

	
	public void adicionarPaciente(Paciente umPaciente){
		listaPacientes.add(umPaciente);
	}
	
	public void adicionarMedico(Medico umMedico){
		listaMedicos.add(umMedico);
	}
	
	
	public void adicionarConsulta(ConsultaMedica umaConsulta){
		listaConsulta.add(umaConsulta);
	}	
	
	public void inserirDadosConsulta(ConsultaMedica umaConsulta, int numero, Medico umMedico, Paciente umPaciente, String diagnostico){
		umaConsulta.setNumero(numero);
		umaConsulta.setDiagnostico(diagnostico);
		umaConsulta.setMedico(umMedico);
		umaConsulta.setPaciente(umPaciente);
	}		
	
	
	public boolean removerPaciente(Paciente umPaciente){
		if (umPaciente == null)
		{ 
			return false;
		}
		else
		{
		listaPacientes.remove(umPaciente);
			return true;
		}
	}

	public boolean removerMedico(Medico umMedico){
		if (umMedico == null)
		{ 
			return false;
		}
		else
		{
		listaMedicos.remove(umMedico);
			return true;
		}
	}
	

	public boolean removerConsulta(ConsultaMedica umaConsulta){
		if (umaConsulta == null)
		{ 
			return false;
		}
		else
		{
		listaConsulta.remove(umaConsulta);
			return true;
		}
	}
	
	
	public Paciente pesquisarPaciente(String umNome){
		for(Paciente umPaciente : listaPacientes){
			if(umPaciente.getNome().equalsIgnoreCase(umNome))
			return umPaciente;
		}
		return null;
	}
	
	
	public Medico pesquisarMedico(String umNome){
		for(Medico umMedico : listaMedicos){
			if(umMedico.getNome().equalsIgnoreCase(umNome))
			return umMedico;
		}
		return null;
	}

	
	public ConsultaMedica pesquisarConsulta(int umNumero){
		for(ConsultaMedica umaConsulta : listaConsulta){
			if(umNumero == umaConsulta.getNumero() );
			return umaConsulta;
		}
		return null;
	}
	
	
	public String exibirPacientes(){
		String saida= "Pacientes:";
		if(listaPacientes.size()>0){
		for(Paciente umPaciente : listaPacientes){
			saida = (saida + "\n" + umPaciente.getNome());
		} 
		}
		return saida; 
	}	
	
	public String exibirMedicos(){
		String saida= "Medicos:";
		if(listaMedicos.size()>0){
		for(Medico umMedico : listaMedicos){
			saida = (saida + "\n" + umMedico.getNome());
		} 
		}
		return saida; 
	}
	
	public String exibirConsulta(){
		String saida= "Consultas:";
		if(listaConsulta.size()>0){
		for(ConsultaMedica umaConsulta : listaConsulta){
			saida = (saida + "\n" + umaConsulta.getNumero());
		} 
		}
		return saida; 
	}

	

	public String caractPaciente(Paciente umPaciente){
		
		String Pac= ("  Paciente"+"\n"+ "Nome: " + umPaciente.getNome() + "\n" + "Idade: " + umPaciente.getIdade()+ "\n" +"Peso: " + umPaciente.getPeso() + "\n"+"Plano de Saude: " + umPaciente.getPlanoSaude() + "\n" + "Doenca: " + umPaciente.getDoenca() + "\n"+ "Inicio do Tratamento: " + umPaciente.getInicioTratamento() + "\n"+ "Medicamento: " + umPaciente.getMedicamentoUtilizado() + "\n");
		return Pac;
	}
	
	public String caractMedico(Medico umMedico){
		
		String Med= ("  Medico"+"\n"+ "Nome: " + umMedico.getNome() + "\n" + "Idade: " + umMedico.getIdade()+ "\n" +"Peso: " + umMedico.getPeso() + "\n" +"CRM: " + umMedico.getCrm() + "\n" +"Anos de Experiencia: " + umMedico.getAnosExperiencia() + "\n" +"Hospital em que trabalha: " + umMedico.getHospitalTrabalho() + "\n");
		return Med;
	}
	
	public String caractConsulta(ConsultaMedica umConsulta){
		
		String Con= ("  Consulta"+"\n"+ "Numero: " + umConsulta.getNumero()+ "\n" + "Paciente: " + umConsulta.getPaciente().getNome()+ "\n"+ "Medico: " + umConsulta.getMedico().getNome()+ "\n"+ "Numero da Consulta: " + umConsulta.getNumero()+ "\n"+ "Diagnóstico: " + umConsulta.getDiagnostico()+ "\n"+ "Dia da Consulta: " + umConsulta.getDia()+ "\n"+ "Medicamento Indicado: " + umConsulta.getMedicamentoIndicado()+ "\n");
		return Con;
	}
	
}
